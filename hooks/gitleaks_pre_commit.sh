#!/bin/bash

# Path to the Windows executable
EXE_PATH="gitleaks.exe"
EXE_OPTIONS="detect --no-git --verbose --config="gitleaks-config.toml""
$EXE_PATH $EXE_OPTIONS
output="$($EXE_PATH $EXE_OPTIONS)"

# Check if the output contains leaks
if echo "$output" | grep "Finding"; then
    echo "secrets found"
    exit 1
else
    echo "secrets not found"
    exit 0
fi

